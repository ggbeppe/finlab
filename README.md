# FinLab

Usage:
1. Install docker and docker-compose.
2. Run
```$xslt
docker-compose -f docker-compose-dev.yml up -d --build
```
In order to stop, run
```$xslt
docker-compose -f docker-compose-dev.yml stop
```
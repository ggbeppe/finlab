import React from 'react';
import {Container, Dropdown, Image, Menu as MyMenu, Responsive} from "semantic-ui-react";

import myImage from '../../../assets/images/logo/TradeSquare_420.jpg';
import styles from './Menu.module.css'

const Menu = () => {
  return (
    <Container>
      <MyMenu secondary size={'huge'}>
        <MyMenu.Item position={'left'} className={styles.logo}>
          <Image src={ myImage } size={'tiny'} circular href={'/register'} />
        </MyMenu.Item>

        <MyMenu.Item position={'right'}>

          <Responsive minWidth={768}>
            <MyMenu.Menu position={'right'} >
              <MyMenu.Item name='About Us' position="right" href={'/register'} />
              <MyMenu.Item name='Contacts' position="right" href={'/register'} />
            </MyMenu.Menu>
          </Responsive>

          <Responsive maxWidth={767} >
            <Dropdown className='icon' item size={'huge'} icon={'bars'} direction={'left'} floating>
              <Dropdown.Menu>
                <Dropdown.Item href={'/register'}>About Us</Dropdown.Item>
                <Dropdown.Item href={'/register'}>Contacts</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Responsive>

        </MyMenu.Item>

      </MyMenu>
    </Container>
  )
};

export default Menu;
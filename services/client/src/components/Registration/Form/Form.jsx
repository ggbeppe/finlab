import React from "react";
import {Button, Form, Grid, Header, Menu as MyMenu, Responsive} from "semantic-ui-react";

import styles from './Form.module.css'

const RegistrationForm = (props) => {

  return (
    <Form onSubmit={props.handleSubmit}>
      <Grid
        divided='vertically'
        container >

        <Grid.Row>
          <Grid.Column>
            <Header as={'h4'}>Create you Personal Account</Header>
            <Form.Input
              label={'Username'}
              name={'username'}
              placeholder={'Username'}
              required
              onChange={props.handleChange}/>
            <Form.Input
              label={'Email'}
              type={'email'}
              name={'email'}
              placeholder={'e.g. "john.doe@example.com"'}
              required
              onChange={props.handleChange} />
            <Form.Input
              label={'Password'}
              type={'password'}
              name={'password'}
              placeholder={'Password'}
              required
              onChange={props.handleChange}/>
          </Grid.Column>

        </Grid.Row>


        <Grid.Row >

          <Responsive minWidth={500}>
            <Grid.Column >
              <Grid className={styles.submit_no_mobile}>
                <Grid.Column width={5} >
                  <Form.Button
                    color={'teal'}
                    control={Button}
                    type={'submit'} >Create an Account</Form.Button>
                </Grid.Column>

                <Grid.Column width={11} >
                  <label>
                    By proceeding you agree to our <a href='/register'>Terms of Service</a> and <a href='/register'>Privacy Statement</a>. We’ll occasionally send you account-related emails.
                  </label>
                </Grid.Column>
              </Grid>
            </Grid.Column>
          </Responsive>

          <Responsive maxWidth={499}>
            <Grid.Column className={styles.submit_mobile}>
              <Form.Button
                color={'teal'}
                control={Button}
                type={'submit'} >Create an Account</Form.Button>
              <label>
                By proceeding you agree to our <a href='/register'>Terms of Service</a> and <a href='/register'>Privacy Statement</a>. We’ll occasionally send you account-related emails.
              </label>
            </Grid.Column>
          </Responsive>

        </Grid.Row>

      </Grid>
    </Form>
  );

};

export default RegistrationForm
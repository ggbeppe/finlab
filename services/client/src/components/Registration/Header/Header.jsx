import React from 'react';
import { Container, Header as MyHeader } from "semantic-ui-react";
import styles from './Header.module.css'


const content = {
  mainHeader: 'Join TradeSquare',
  subHeader: 'The best way to design, test and ship financial products.'
};

const Header = () => {

    return (
      <Container className={styles.container} >
        <MyHeader as="h1" dividing>
          {content.mainHeader}
          <MyHeader  sub={true} >
            {content.subHeader}
          </MyHeader>
        </MyHeader>
      </Container>
    );
};

export default Header;
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import './MainHeader.module.css';

class MainHeader extends Component {

    render() {
        const { user } = this.props;
        const sellingLinks = (
            <Fragment>
              <a href="#">Why Finlab?</a>
              <a href="#">Pricing</a>
            </Fragment>
        );
        const authButtons = (
            <div className="header-right">
                <button className="ui inverted button">Sign In</button>
                <button className="ui teal button">Sign Up</button>
            </div>);

        const profileIcon = (
            <div className="header-profile">
                <img src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50" alt="Avatar"/>
            </div>);


        return (
            <div className="main-header">
                <div className="ui container">
                    <div className="logo">
                        <i className="icon flask inverted big"/>
                    </div>
                    { user ? null : sellingLinks}
                    { user ? null : authButtons}
                    { user ? profileIcon : null}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
};

export default connect(mapStateToProps)(MainHeader);

import React from 'react';

import MainHeader from '../Navigation/MainHeader/MainHeader'

const Layout = (props) => {
    return (
        <div>
            <MainHeader />
            {props.children}
        </div>);
};

export default Layout;

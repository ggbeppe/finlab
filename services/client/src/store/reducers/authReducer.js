import { CREATE_USER } from "../actions/types";

export default (state = null, action) => {
    switch (action.type) {
        case CREATE_USER:
            return {
                ...state,
                user: action.payload.user
            };
        default:
            return state;
    }
};
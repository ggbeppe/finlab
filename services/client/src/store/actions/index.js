import axios from 'axios';
import { CREATE_USER } from './types';

export const createUser = ({ username, email, password, history }) => async dispatch => {
    try {
        const { data } = await axios.post('http://localhost:5000/auth/register', { username, email, password });
        dispatch({ type: CREATE_USER, payload: data.user });
        history.push('/');
    } catch (e) {
        console.log('Error creating user', e);
    }
};
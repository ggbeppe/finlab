import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import LoginScreen from './Login/Login';
import RegisterScreen from './Registration/Registration';
import RegisterScreen_old from './Registration/Registration-old';
import Dashboard from './Dashboard/Dashboard';
import PrivateRoute from "../components/PrivateRouter/PrivateRoute";

const Root = () => {
  return (
    <div>
      <Router>
        <Switch>
          <Route path='/login' component={LoginScreen} />
          <Route path='/register' component={RegisterScreen} />
          <Route path='/register_old' component={RegisterScreen_old} />
          <PrivateRoute path="/" component={Dashboard} />
        </Switch>
      </Router>
    </div>
  );
};

export default Root;

import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import {Container, Grid} from "semantic-ui-react";
import _ from 'lodash';

import Header from "../../components/Registration/Header/Header";
import Menu from "../../components/Registration/Menu/Menu";
import Form from "../../components/Registration/Form/Form";


class RegisterScreen extends Component {
  constructor(props) {
    super(props);

    console.log(props);

    this.state = {
      username: '',
      email: '',
      password: '',
      usernameError: '',
      emailError: '',
      passwordError: '',
      loading: false,
    };
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value, [event.target.name + 'Error']: '' });
  };

  isFormValid = (values) => {
    let errors = {};

    if (values.username.length === 0) {
      errors.usernameError = 'This field can not be blank';
    }
    if (!/.+@.+\.[A-Za-z]+$/.test(values.email)) {
      errors.emailError = 'Email is invalid or already taken';
    }
    if (values.password.length < 8) {
      errors.passwordError = 'The password must be at least 8 characters';
    }
    if (Object.keys(errors).length > 0) {
      this.setState({...errors});
      return false;
    }
    return true;
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (event.target){
      const nodeList = _.map(event.target.querySelectorAll('input'), el => el);
      const values = _.mapValues(_.keyBy(nodeList, 'name'), 'value');

      if(this.isFormValid(values)){
        values.history = this.props.history;

        console.log(values);
        this.props.createUser(values);
      }
    }
  };


  render() {
    return (
      <Fragment>
        <Container>
          <Menu />
          <Header />

          <Grid columns={3} container>
            <Grid.Column computer={8} tablet={10} mobile={15}>
              <Form handleChange={this.handleChange} handleSubmit={this.handleSubmit}/>
            </Grid.Column>
            <Grid.Column computer={2} only={'computer'}/>
            <Grid.Column width={6} only={'computer tablet'}/>
          </Grid>
        </Container>
      </Fragment>
    )
  }
};


export default connect(null, actions)(RegisterScreen);
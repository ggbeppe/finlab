import React, { Component } from 'react';

import { connect } from 'react-redux';
import * as actions from '../../store/actions';

class RegisterScreen_old extends Component {
    state = {
        username: '',
        email: '',
        password: '',
        usernameError: '',
        emailError: '',
        passwordError: '',
        loading: false,
    };

    isFormValid = () => {
        let errors = {};
        if (this.state.username.length === 0) {
            errors.usernameError = 'This field can not be blank';
        }
        if (!/.+@.+\.[A-Za-z]+$/.test(this.state.email)) {
            errors.emailError = 'Email is invalid or already taken';
        }
        if (this.state.password.length < 8) {
            errors.passwordError = 'The password must be at least 8 characters';
        }
        if (Object.keys(errors).length > 0) {
            this.setState({...errors});
            return false;
        }
        return true;
    };

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value, [event.target.name + 'Error']: '' });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.isFormValid();
        if (this.isFormValid()) {
            this.props.createUser({
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                history: this.props.history
            });
        }

    };

    render() {
        return (
            <div className="ui container">
                <h1 className="ui container" style={{margin: '50px 0'}}>
                    Join Finlab
                    <div className="sub container">The best way to design, test and ship financial products.</div>
                </h1>
                <div className="ui grid">
                    <div className="eight wide column">
                        <form className="ui form" onSubmit={this.handleSubmit}>
                            <h4 className="ui dividing container" style={{marginBottom: '30px'}}>Create you Personal Account</h4>
                            <div className="field">
                                <label>Username <span style={{color: 'red'}}>*</span></label>
                                <input onChange={this.handleChange} type="text" name="username" placeholder="Username" value={this.state.username}/>
                                {this.state.usernameError ? (<div className="ui pointing red basic label">
                                    {this.state.usernameError}
                                </div>) : null}
                            </div>
                            <div className="field">
                                <label>Email <span style={{color: 'red'}}>*</span></label>
                                <input onChange={this.handleChange} type="email" name="email" placeholder="Email Address" value={this.state.email}/>
                                {this.state.emailError ? (<div className="ui pointing red basic label">
                                    {this.state.emailError}
                                </div>) : null}
                            </div>
                            <div className="field">
                                <label>Password <span style={{color: 'red'}}>*</span></label>
                                <input onChange={this.handleChange} type="password" name="password" placeholder="Password" value={this.state.password}/>
                                {this.state.passwordError ? (<div className="ui pointing red basic label">
                                    {this.state.passwordError}
                                </div>) : null}
                            </div>
                            <div className="ui divider" style={{marginTop: '30px'}}></div>
                            <p>
                                By clicking "Create an account" below, you agree to our <a href="#">Terms of Service</a> and <a
                                href="#">Privacy Statement</a>.
                                We’ll occasionally send you account-related emails.
                            </p>
                            <div className="ui divider" style={{marginBottom: '30px'}}></div>
                            <button className="ui button teal" tabIndex="0">Create an Account</button>
                        </form>
                    </div>
                    <div className="two wide column"></div>
                    <div className="four wide column">
                        <div className="ui card">
                            <div className="content">
                                <div className="container">You'll love it</div>
                            </div>
                            <div className="content">
                                <ul className="ui list">
                                    <li>Centralize your data</li>
                                    <li>Instant setup</li>
                                    <li>Backtesting at a fingers click</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(null, actions)(RegisterScreen_old);

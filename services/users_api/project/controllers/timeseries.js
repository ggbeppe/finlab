const mongoose = require('mongoose');
const uuidv1 = require('uuid/v1');
const aws = require('aws-sdk');
const { validationResult } = require('express-validator/check');

const TimeSeries = mongoose.model('timeSeries');

exports.fetchSeries = async (req, res) => {
    const series = await TimeSeries.find({ _user: mongoose.Types.ObjectId(req.user) });

    res.json({
        status: 'success',
        data: series
    });
};

exports.uploadSeries = async (req, res) => {

    // Check request parameters
    const response = {
        status: 'fail',
        message: 'TimeSeries upload failed'
    };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        response.errors = errors.array();
        return res.status(400).json(response);
    }

    const { filename, fileExtension } = req.body;
    const { user } = req;

    try {

        const s3 = new aws.S3({
            accessKeyId : process.env.AWS_ACCESS_KEY,
            secretAccessKey : process.env.AWS_SECRET_KEY,
            useAccelerateEndpoint: true
        });

        const Key = `/${user}/timeseries/${uuidv1()}.${fileExtension}`;

        const serie = TimeSeries({
            _user: mongoose.Types.ObjectId(user),
            name: filename,
            path: Key
        });
        await serie.save();

        const params = {
            Bucket: process.env.AWS_BUCKET,
            Key,
            Expires: parseInt(process.env.TIMESERIES_UPLOAD_EXP_TIME),
            ACL: 'bucket-owner-full-control',
            ContentType: fileExtension
        };

        const url = await s3.getSignedUrl('putObject', params);

        res.json({
            status: 'success',
            url: url
        });
    } catch (e) {
        res.status(500).json(response)
    }
};

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator/check');

const User = mongoose.model('users');

exports.signup = async (req, res) => {

    // Check request parameters
    const response = {
        status: 'fail',
        message: 'User creation failed'
    };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        response.errors = errors.array();
        return res.status(400).json(response);
    }

    const { username, email, password } = req.body;

    // Check if email already in use
    let userExists = await User.findOne({ email });
    if (userExists) {
        response.errors = [{
            location: 'body',
            param: 'email',
            msg: 'This email is already taken'
        }];
        return res.status(412).json(response);
    }
    // Check if username already in use
    userExists = await User.findOne({ username });
    if (userExists) {
        response.errors = [{
            location: 'body',
            param: 'username',
            msg: 'This username is already taken'
        }];
        return res.status(412).json(response);
    }

    // Hash password and create user
    const hash = await bcrypt.hash(password, parseInt(process.env.SALT_ROUNDS));
    const user = await User({username, email, password: hash}).save();

    if (user) {
        response.status = 'success';
        response.message = 'User created';
        response.user = user.toJSON();
        return res.status(201).json(response)
    }

    // Internal error
    return res.status(500).json(response);
};

exports.signin = async (req, res) => {

    // Check request parameters
    const response = {
        status: 'fail',
        message: 'User login failed'
    };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        response.errors = errors.array();
        return res.status(400).json(response);
    }

    const { login, password } = req.body;

    // Find user if it exists
    const user = await User.findByLogin(login);
    if (user === null) {
        response.errors = [{
            location: 'body',
            param: 'login',
            msg: 'Incorrect email/username or password'
        }];
        res.status(401).json(response);
    }

    const valid = await bcrypt.compare(password, user.password);

    if (valid === true) {

        const payload = { user: user.id };
        const signOptions = { expiresIn: process.env.TOKEN_EXP_DAYS };
        const authToken = await jwt.sign(payload, process.env.SECRET_KEY, signOptions);

        if (authToken === null) {
            return res.status(500).json(response);
        }

        response.status = 'success';
        response.message = 'User login success';
        response.authToken = authToken;
        return res.json(response);

    } else {
        response.errors = [{
            location: 'body',
            param: 'login',
            msg: 'Incorrect email/username or password'
        }];
        return res.status(401).json(response);
    }
};

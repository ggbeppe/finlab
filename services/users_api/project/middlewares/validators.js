const { check, body } = require('express-validator/check');

exports.validate = (method) => {
    switch (method) {
        case 'createUser': {
            return [
                body('username', 'Enter a valid username').exists(),
                body('email', 'Enter a valid email address').isEmail(),
                body('password', 'Enter a valid password of at least 8 characters').isLength({ min: 8 })
            ];
        }
        case 'loginUser': {
            return [
                body('login', 'Enter a valid username or email').exists(),
                body('password', 'Enter a valid password of at least 8 characters').isLength({ min: 8 })
            ];
        }
        case 'uploadSeries': {
            return [
                body('filename', 'Enter a valid filename of at least 2').isLength({ min: 2 }),
                body('fileExtension', 'Enter a valid extension format').exists()
            ];
        }
    }
};

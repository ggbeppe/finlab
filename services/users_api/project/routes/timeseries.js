const express = require('express');
const router = express.Router();
const validators = require('../middlewares/validators');
const auth = require('../middlewares/auth');

const { fetchSeries, uploadSeries } = require('../controllers/timeseries');

router.get('/', auth, fetchSeries);
router.post('/upload', [auth, [validators.validate('uploadSeries')]], uploadSeries);

module.exports = router;

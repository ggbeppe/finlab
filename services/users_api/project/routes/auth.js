const express = require('express');
const router = express.Router();
const validators = require('../middlewares/validators');

const { signup, signin } = require('../controllers/auth');

router.post('/register', validators.validate('createUser'), signup);
router.post('/login', validators.validate('loginUser'), signin);

module.exports = router;

const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const cors = require('cors');
require('dotenv').config();
require('./models');

// import routes
const pingRoutes = require('./routes/ping');
const authRoutes = require('./routes/auth');
const timeseriesRoutes = require('./routes/timeseries');

// app
const app = express();

// connect to db
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useCreateIndex: true
}).then(() => console.log('MongoDB connected'));

// Middleware
app.use(cors());
app.use(morgan(process.env.MORGAN_LOG_FORMAT));
app.use(bodyParser.json());
app.use(expressValidator());

// routes middleware
app.use('/ping', pingRoutes);
app.use('/auth', authRoutes);
app.use('/timeseries', timeseriesRoutes);

const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});

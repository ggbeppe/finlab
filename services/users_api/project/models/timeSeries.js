const mongoose = require('mongoose');
const { Schema } = mongoose;

const timeSeriesSchema = new Schema({
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    name: {
        type: String,
        required: [true, 'cannot be blank']
    },
    path: String,
    fileExtension: String,
    startDate: Date,
    endDate: Date
}, {timestamps: true});

module.exports = mongoose.model('timeSeries', timeSeriesSchema);

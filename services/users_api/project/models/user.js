const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
    firstName: String,
    lastName: String,
    username: {
        type: String,
        required: [true, 'cannot be blank'],
        index: true
    },
    email: {
        type: String,
        required: [true, 'cannot be blank'],
        index: true
    },
    password: String,
    avatar: String,
}, {timestamps: true});

userSchema.methods.toJSON = function() {
    const obj = this.toObject();
    delete obj.password;
    return obj;
};

userSchema.statics.findByLogin = async function (login) {
    let user = await this.findOne({ username: login });

    if (!user) {
        user = await this.findOne({ email: login });
    }
    return user;
};


module.exports = mongoose.model('users', userSchema);

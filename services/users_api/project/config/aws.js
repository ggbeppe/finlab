const aws = require('aws-sdk');

const myaws = aws.config.update({
    accessKeyId : process.env.AWS_ACCESS_KEY,
    secretAccessKey : process.env.AWS_SECRET_KEY,
    useAccelerateEndpoint: false
});

module.exports = { myaws };
